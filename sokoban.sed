#!/bin/sed -f
################################################################################
#
#Copyright 2013, Denis Tikhomirov. All rights reserved.
#
#Redistribution and use in source and binary forms, with or without modification,
#are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this 
#   list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice, 
#   this list of conditions and the following disclaimer in the documentation 
#   and/or other materials provided with the distribution.
#
#THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, 
#INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
#FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
#CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
#OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
#IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
#OF SUCH DAMAGE.
#
# This game uses levels created by Yoshio Murase.
#
################################################################################

#init level
1 {
  #print help
  s/.*/h - left, H - left till the end, k - up, K - up till the end, \n/
  s/.*/&j - down, J - down till the end, l - right, L - right till the end, \n/
  s/.*/&n - next level, x - previous step, \n/
  s/.*/&q - quit.\n\n/
  s/.*/&Script was creatd by twisted mind of Denis Tikhomirov.\n/
  s/.*/&Yoshio Murase's magnificent levels were used.\n\n/
  p
  s/.*//
  #form data
  s/.*/&levels_list\n/
  s/.*/&level\n/
  s/.*/&###################\n/
  s/.*/&#   ...  c  ...   #\n/
  s/.*/&# ooo  #####  ooo #\n/
  s/.*/&##   ###   ###   ##\n/
  s/.*/& ##  #       ##  #\n/
  s/.*/&  ####        ####/
  s/.*/&level_end\n/
  s/.*/&level\n/
  s/.*/&  ####\n/
  s/.*/&###  ##\n/
  s/.*/&#   o #\n/
  s/.*/&# #.#c#\n/
  s/.*/&# #o .#\n/
  s/.*/&#  .o #\n/
  s/.*/&##   ##\n/
  s/.*/& #####/
  s/.*/&level_end\n/
  s/.*/&level\n/
  s/.*/&  #####\n/
  s/.*/&###   #\n/
  s/.*/&# o # ##\n/
  s/.*/&# o.o. #\n/
  s/.*/&# ##.  #\n/
  s/.*/&#  c ###\n/
  s/.*/&######/
  s/.*/&level_end\n/
  s/.*/&level\n/
  s/.*/&  #####\n/
  s/.*/&###   ###\n/
  s/.*/&# . o . #\n/
  s/.*/&# #.o.# #\n/
  s/.*/&# o # o #\n/
  s/.*/&### c ###\n/
  s/.*/&  #####/
  s/.*/&level_end\n/
  s/.*/&level\n/
  s/.*/&  #####\n/
  s/.*/&  #   ###\n/
  s/.*/&###.#   #\n/
  s/.*/&# o.o # #\n/
  s/.*/&# #* o  #\n/
  s/.*/&#c . ####\n/
  s/.*/&######/
  s/.*/&level_end\n/
  s/.*/&input\n/
  s/.*/&\n/
  s/.*/&current_level\n/
  s/.*/&prev_steps\n/
  s/.*/&end\n/

  b load_next_level
}

b after_load_next_level

:load_next_level

t clear_flag_level
:clear_flag_level;
#move first level from list to current_level
s/\(levels_list\n\)level\n\([^\(level\)]*\)level_end\n\(.*current_level\n\)[^\(prev_steps\)]*/\1\3\2/

#if we can load next level - game is not finished
t after_win_check
#otherwise - it is
b win

:after_win_check

#and copy it to previous step
s/\(current_level\n\)\([^\(prev_steps\)]*\)\(prev_steps\n\)[^\(end\)]*/\1\2\3step\n\2step_end\n/

#put data to hold and clear pattern space
x; s/.*//

#go to end of script
#b after_complete

:after_load_next_level

#place input into data
#append data to pattern space...
G
#...and place input in proper position and save data
s/^\(.*\)\(levels_list.*input\n\)[^\n]*\n/\2\1/
h

:extract_level
#extract current level and put it into hold
s/.*current_level\n\([^\(prev_steps\)]*\).*/\1/
x

:process_input

t clear_flag
:clear_flag;

#go right
/input\n[lL]/ {
  :process_r
  #get level
  x

  #try move [crate] to floor
  s/\([cC]\)\(o\{0,1\}\) /\1c\2/
  t after_move_r

  #try move active crate to floor
  s/\([cC]\)a /\1Co/
  t after_move_r

  #try move crate to crate cell
  s/\([cC]\)o\./\1ca/
  t after_move_r

  #try move [active crate] to crate cell
  s/\([cC]\)\(a\{0,1\}\)\./\1C\2/
  t after_move_r

  :after_move_r
  #Replace previous character with crate cell or floor
  s/C\([cC]\)/\.\1/
  t check_go_till_end_r
  s/c\([cC]\)/ \1/
  t check_go_till_end_r

  b after_check_go_till_end_r

  #if go-till-end command used, repeat
  :check_go_till_end_r
  x
  s/input\n[A-Z]/&/
  t process_r
  x

  :after_check_go_till_end_r

  #save level
  x
  #append updated level to data
  G
  #then set current level and previous step
  s/\(current_level\n\)\([^\(prev_steps\)]*\)\(prev_steps\n.*\)\(end\n\)\n\(.*\)/\1\5\3step\n\2step_end\n\4/
  #continue input process
  s/\(input\n\)./\1/;
  b process_input
}

#go left
/input\n[hH]/ {
  :process_l
  #get level
  x

  #try move [crate] to floor
  s/ \(o\{0,1\}\)\([cC]\)/\1c\2/
  t after_move_l

  #try move active crate to floor
  s/ a\([cC]\)/oC\1/
  t after_move_l

  #try move crate to crate cell
  s/\.o\([cC]\)/ac\1/
  t after_move_l

  #try move [active crate] to crate cell
  s/\.\(a\{0,1\}\)\([cC]\)/\1C\2/
  t after_move_l

  :after_move_l
  #Replace previous character with crate cell or floor
  s/\([cC]\)C/\1\./
  t check_go_till_end_l
  s/\([cC]\)c/\1 /
  t check_go_till_end_l

  b after_check_go_till_end_l

  #if go-till-end command used, repeat
  :check_go_till_end_l
  x
  s/input\n[A-Z]/&/
  t process_l
  x

  :after_check_go_till_end_l

  #save level
  x
  #append updated level to data
  G
  #then set current level and previous step
  s/\(current_level\n\)\([^\(prev_steps\)]*\)\(prev_steps\n.*\)\(end\n\)\n\(.*\)/\1\5\3step\n\2step_end\n\4/
  #continue input process
  s/\(input\n\)./\1/;
  b process_input
}

#go down
/input\n[jJ]/ {
  :process_d
  #get level
  x

  :start_d {
    t clear_flag_d
    :clear_flag_d;

    #try replace first wall in char's row
    s/\(.*\n[vwxyz]*\)#\([^\n]*[cC]\)/\1x\2/
    t try_replace_below_char_d

    #try replace first floor in char's row
    s/\(.*\n[vwxyz]*\) \([^\n]*[cC]\)/\1y\2/
    t try_replace_below_char_d

    #try replace first crate in char's row
    s/\(.*\n[vwxyz]*\)o\([^\n]*[cC]\)/\1z\2/
    t try_replace_below_char_d

    #try replace first active crate in char's row
    s/\(.*\n[vwxyz]*\)a\([^\n]*[cC]\)/\1w\2/
    t try_replace_below_char_d

    #try replace first crate cell in char's row
    s/\(.*\n[vwxyz]*\)\.\([^\n]*[cC]\)/\1v\2/
    t try_replace_below_char_d

    #nothing was replaced, go to the end of procedure
    b end_d

    #perform replaces on row below character
    :try_replace_below_char_d
    #try replace first wall below char's row
    s/\([cC][^\n]*\n[vwxyz]*\)#/\1x/
    t chk_row_below_has_crate_d

    #try replace first flor below char's row
    s/\([cC][^\n]*\n[vwxyz]*\) /\1y/
    t chk_row_below_has_crate_d

    #try replace first crate below char's row
    s/\([cC][^\n]*\n[vwxyz]*\)o/\1z/
    t chk_row_below_has_crate_d

    #try replace first active crate below char's row
    s/\([cC][^\n]*\n[vwxyz]*\)a/\1w/
    t chk_row_below_has_crate_d

    #try replace first crate cell below char's row
    s/\([cC][^\n]*\n[vwxyz]*\)\./\1v/
    t chk_row_below_has_crate_d
      
    #nothing was replaced, go to the end of procedure
    b end_d
    
    #check if there is a crate in a row below char
    :chk_row_below_has_crate_d
    s/[cC][^\n]*\n[^\n]*[oa]/&/
    t try_replace_below_crate_d
    
    #there is no crate, go to the beginning
    b start_d

    #perform replaces on row below crate below char
    :try_replace_below_crate_d
    #try replace first wall below crate's row
    s/\([cC][^\n]*\n[^\n]*\n[vwxyz]*\)#/\1x/
    t start_d

    #try replace first floor below crate's row
    s/\([cC][^\n]*\n[^\n]*\n[vwxyz]*\) /\1y/
    t start_d

    #try replace first crate below crate's row
    s/\([cC][^\n]*\n[^\n]*\n[vwxyz]*\)o/\1z/
    t start_d

    #try replace first active crate below crate's row
    s/\([cC][^\n]*\n[^\n]*\n[vwxyz]*\)a/\1w/
    t start_d

    #try replace first crate cell below crate's row
    s/\([cC][^\n]*\n[^\n]*\n[vwxyz]*\)\./\1v/
    #if we can't replace row above crate - it's ok
    b start_d

    :end_d;
  }

  #try to move crate first
  #try to move crate to floor
  s/\([vwxyz]\+[oa][^\n]*\n[vwxyz]\+\) /\1o/
  t move_char_to_crate_d

  #try to move crate to crate cell
  s/\([vwxyz]\+[oa][^\n]*\n[vwxyz]*\)\./\1a/
  t move_char_to_crate_d

  b move_char_d

  :move_char_to_crate_d
  #try to move character to previous crate
  s/\([cC][^\n]*\n[vwxyz]*\)o/\1c/
  t replace_back_d

  #try to move character to previous active crate
  s/\([cC][^\n]*\n[vwxyz]*\)a/\1C/
  t replace_back_d

  #move character
  :move_char_d
  #try to move character to floor
  s/\([cC][^\n]*\n[vwxyz]*\) /\1c/
  t replace_back_d

  #try to move character to crate cell
  s/\([cC][^\n]*\n[vwxyz]*\)\./\1C/
  t replace_back_d

  :replace_back_d
  #Replace previous character with crate cell or floor
  s/C\([^\n]*\n[vwxyz]*[cC]\)/\.\1/
  t check_go_till_end_d
  s/c\([^\n]*\n[vwxyz]*[cC]\)/ \1/
  t check_go_till_end_d

  b after_check_go_till_end_d

  #if go-till-end command used, repeat
  :check_go_till_end_d
  #set back replaced cells
  y/vwxyz/.a# o/

  x
  s/input\n[A-Z]/&/
  t process_d
  x

  :after_check_go_till_end_d

  #set back replaced cells
  y/vwxyz/.a# o/

  #save level
  x
  #append updated level to data
  G
  #then set current level and previous step
  s/\(current_level\n\)\([^\(prev_steps\)]*\)\(prev_steps\n.*\)\(end\n\)\n\(.*\)/\1\5\3step\n\2step_end\n\4/
  #continue input process
  s/\(input\n\)./\1/;
  b process_input
}

#go up
/input\n[kK]/ {
  :process_u
  #get level
  x

  :start_u {
    t clear_flag_u
    :clear_flag_u;

    #try replace first wall in char's row
    s/\(.*\n[vwxyz]*\)#\([^\n]*[cC]\)/\1x\2/
    t try_replace_above_char_u

    #try replace first floor in char's row
    s/\(.*\n[vwxyz]*\) \([^\n]*[cC]\)/\1y\2/
    t try_replace_above_char_u

    #try replace first crate in char's row
    s/\(.*\n[vwxyz]*\)o\([^\n]*[cC]\)/\1z\2/
    t try_replace_above_char_u

    #try replace first active crate in char's row
    s/\(.*\n[vwxyz]*\)a\([^\n]*[cC]\)/\1w\2/
    t try_replace_above_char_u

    #try replace first crate cell in char's row
    s/\(.*\n[vwxyz]*\)\.\([^\n]*[cC]\)/\1v\2/
    t try_replace_above_char_u

    #nothing was replaced, go to the end of procedure
    b end_u

    #perform replaces on row above character
    :try_replace_above_char_u
    #try replace first wall above char's row
    s/\(\n[vwxyz]*\)#\([^\n]*\n[^\n]*[cC]\)/\1x\2/
    t chk_row_above_has_crate_u

    #try replace first flor above char's row
    s/\(\n[vwxyz]*\) \([^\n]*\n[^\n]*[cC]\)/\1y\2/
    t chk_row_above_has_crate_u

    #try replace first crate above char's row
    s/\(\n[vwxyz]*\)o\([^\n]*\n[^\n]*[cC]\)/\1z\2/
    t chk_row_above_has_crate_u

    #try replace first active crate above char's row
    s/\(\n[vwxyz]*\)a\([^\n]*\n[^\n]*[cC]\)/\1w\2/
    t chk_row_above_has_crate_u

    #try replace first crate cell above char's row
    s/\(\n[vwxyz]*\)\.\([^\n]*\n[^\n]*[cC]\)/\1v\2/
    t chk_row_above_has_crate_u
      
    #nothing was replaced, go to the end of procedure
    b end_u
    
    #check if there is a crate in a row above char
    :chk_row_above_has_crate_u
    s/[oa][^\n]*\n[^\n]*[cC]/&/
    t try_replace_above_crate_u
    
    #there is no crate, go to the beginning
    b start_u

    #perform replaces on row above crate above char
    :try_replace_above_crate_u
    #try replace first wall above crate's row
    s/\(\n[vwxyz]*\)#\([^\n]*\n[^\n]*\n[^\n]*[cC]\)/\1x\2/
    t start_u

    #try replace first floor above crate's row
    s/\(\n[vwxyz]*\) \([^\n]*\n[^\n]*\n[^\n]*[cC]\)/\1y\2/
    t start_u

    #try replace first crate above crate's row
    s/\(\n[vwxyz]*\)o\([^\n]*\n[^\n]*\n[^\n]*[cC]\)/\1z\2/
    t start_u

    #try replace first active crate above crate's row
    s/\(\n[vwxyz]*\)a\([^\n]*\n[^\n]*\n[^\n]*[cC]\)/\1w\2/
    t start_u

    #try replace first crate cell above crate's row
    s/\(\n[vwxyz]*\)\.\([^\n]*\n[^\n]*\n[^\n]*[cC]\)/\1v\2/
    #if we can't replace row above crate - it's ok
    b start_u

    :end_u;
  }

  #try to move crate first
  #try to move crate to floor
  s/\([vwxyz]\+\) \([^\n]*\n[vwxyz]*[oa]\)/\1o\2/
  t move_char_to_crate_u

  #try to move crate to crate cell
  s/\([vwxyz]\+\)\.\([^\n]*\n[vwxyz]*[oa]\)/\1a\2/
  t move_char_to_crate_u

  b move_char_u

  :move_char_to_crate_u
  #try to move character to previous crate
  s/\([vwxyz]\+\)o\([^\n]*\n[vwxyz]*[cC]\)/\1c\2/
  t replace_back_u

  #try to move character to previous active crate
  s/\([vwxyz]\+\)a\([^\n]*\n[vwxyz]*[cC]\)/\1C\2/
  t replace_back_u

  #move character
  :move_char_u
  #try to move character to floor
  s/\([vwxyz]\+\) \([^\n]*\n[vwxyz]*[cC]\)/\1c\2/
  t replace_back_u

  #try to move character to crate cell
  s/\([vwxyz]\+\)\.\([^\n]*\n[vwxyz]*[cC]\)/\1C\2/
  t replace_back_u

  :replace_back_u
  #Replace previous character with crate cell or floor
  s/\([cC][^\n]*\n[vwxyz]*\)C/\1\./
  t check_go_till_end_u
  s/\([cC][^\n]*\n[vwxyz]*\)c/\1 /
  t check_go_till_end_u

  b after_check_go_till_end_u

  #if go-till-end command used, repeat
  :check_go_till_end_u
  #set back replaced cells
  y/vwxyz/.a# o/

  x
  s/input\n[A-Z]/&/
  t process_u
  x

  :after_check_go_till_end_u

  #set back replaced cells
  y/vwxyz/.a# o/

  #save level
  x
  #append updated level to data
  G
  #then set current level and previous step
  s/\(current_level\n\)\([^\(prev_steps\)]*\)\(prev_steps\n.*\)\(end\n\)\n\(.*\)/\1\5\3step\n\2step_end\n\4/
  #continue input process
  s/\(input\n\)./\1/;
  b process_input
}

#quit
/input\nq/ {
  s/.*/Bye!/
  q
}

#go to next level
/input\nn/ {
  s/\(input\n\)./\1/;
  b load_next_level
}

#go to previous step
/input\nx/ {
  s/\(input\n\)./\1/;
  #restore prevoius step
  s/\(current_level\n\)\([^\(prev_steps\)]*\)\(prev_steps\n.*\)step\n\([^\(step_end\)]\+\)step_end\n/\1\4\3/
  h
  b extract_level
}

#get level and print it
x; s/[cC]/@/; p;

#check if level is completed
t clear_flag_complete
:clear_flag_complete

#if any unplaced crates left - level not finished
s/o/o/;
t after_complete

#otherwise, load next level
s/.*/Level completed!\n/;p
x
b load_next_level

:after_complete
d

:win
s/.*/Congratulations, you won!/
q
