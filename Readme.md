## Sokoban game, implemented in sed.

To start game, run `./sokoban.sed` or `sed -f sokoban.sed`and press `Enter`.  
To use commands, type them and press `Enter`.  
You can write multiple commands, and they will be processed at once.

This game uses levels created by Yoshio Murase.

### List of commands:
* `h` - make one step left;
* `H` - go left until it is possible;
* `k` - make one step up;
* `K` - go up until it is possible;
* `j` - make one step down;
* `J` - go down until it is possible;
* `l` - make one step right;
* `L` - go right until it is possible;
* `n` - load next level;
* `x` - restore previous step;
* `q` - quit;
